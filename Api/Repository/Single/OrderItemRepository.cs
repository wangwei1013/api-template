﻿using SqlSugar;
using Model;

namespace Repository;

public class OrderItemRepository : BaseRepository<OrderItem>, IDbAccess
{
    public OrderItemRepository(ISqlSugarClient db) : base(db)
    {
    }
}