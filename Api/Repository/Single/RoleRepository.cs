﻿using Model;
using SqlSugar;

namespace Repository
{
    public class RoleRepository : BaseRepository<SysRole>, IDbAccess
    {
        public RoleRepository(ISqlSugarClient db) : base(db)
        {
        }
    }
}