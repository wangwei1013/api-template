﻿using SqlSugar;
using Model;

namespace Repository;

public class OrderRepository : BaseRepository<Order>, IDbAccess
{
    public OrderRepository(ISqlSugarClient db) : base(db)
    {
    }
}