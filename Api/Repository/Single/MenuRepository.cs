﻿using Model;
using SqlSugar;

namespace Repository
{
    public class MenuRepository : BaseRepository<SysMenu>, IDbAccess
    {
        public MenuRepository(ISqlSugarClient db) : base(db)
        {
        }
    }
}