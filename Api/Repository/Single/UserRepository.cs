﻿using Model;
using SqlSugar;

namespace Repository
{
    public class UserRepository : BaseRepository<SysUser>, IDbAccess
    {
        public UserRepository(ISqlSugarClient db) : base(db)
        {
        }
    }
}