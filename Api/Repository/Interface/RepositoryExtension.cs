﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SqlSugar;
using Model;

namespace Repository;

public static class RepositoryExtension
{
    public static IServiceCollection AddCustomSqlSugar(this IServiceCollection services, IConfiguration config)
    {
        DbConfigModel dbConfig = new DbConfigModel();
        config.GetSection("DbConfig").Bind(dbConfig);
        var sugar = new SqlSugarScope(new ConnectionConfig()
        {
            DbType = dbConfig.DbType,
            ConnectionString = dbConfig.ConnectionString,
            IsAutoCloseConnection = true,
            ConfigureExternalServices = new ConfigureExternalServices
            {
                EntityService = SetDefaultAttr,
            }
        });
        #if DEBUG
        Console.WriteLine("开始数据库检查");
        var stopwatch = Stopwatch.StartNew();
        DbCheck(sugar);
        Console.WriteLine($"结束数据库检查，用时{stopwatch.ElapsedMilliseconds} ms");
        stopwatch.Stop();
        #endif
        services.AddSingleton<ISqlSugarClient, SqlSugarScope>(_ => sugar);
        services.AddTransient(typeof(IBaseRepository<>), typeof(BaseRepository<>));
        //批量注入 具体Repository
        var types = Assembly.GetExecutingAssembly().GetTypes()
            .Where(p => typeof(IDbAccess).IsAssignableFrom(p) && p.IsClass && !p.IsAbstract);
        foreach (var t in types)
        {
            services.AddTransient(t);
        }
        return services;
    }

    private static void SetDefaultAttr(PropertyInfo c, EntityColumnInfo p)
    {
        if (c.PropertyType.IsGenericType &&
            c.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
        {
            p.IsNullable = true;
        }
        //string类型也标识为可空
        if (c.PropertyType == typeof(string))
        {
            p.IsNullable = true;
            if (p.Length == default)
            {
                p.Length = 255;
            }
        }
    }

    private static void DbCheck(ISqlSugarClient db)
    {
        // return;
        // 检查数据库的实体
        var entity = typeof(Entity);
        var types = AppDomain.CurrentDomain.GetAssemblies()
            .SelectMany(s => s.GetTypes())
            .Where(p => (entity.IsAssignableFrom(p)) && p.IsClass && !p.IsAbstract)
            .Where(p => !p.Name.ToLower().EndsWith("entity"))
            .ToArray();
        db.CodeFirst.InitTables(types);
    }
}