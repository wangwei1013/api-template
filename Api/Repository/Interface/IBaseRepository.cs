﻿using System.Linq.Expressions;
using SqlSugar;

namespace Repository;

public interface IBaseRepository<T> where T : class, new()
{
    /// <summary>
    /// 根据实体的主键Id批量更新实体，只更新选中列
    /// </summary>
    /// <param name="selectExp">选中列</param>
    /// <param name="models"></param>
    /// <returns></returns>
    Task<int> UpdateAsync(Expression<Func<T, object>> selectExp = null, params T[] models);

    /// <summary>
    /// 根据条件列（whereExp）批量更新实体，只更新选中列（selectExp）
    /// </summary>
    /// <param name="whereExp">根据此条件列更新</param>
    /// <param name="updateExp">需要更新的列</param>
    /// <param name="models"></param>
    /// <returns></returns>
    Task<int> UpdateAsync(Expression<Func<T, object>> whereExp, Expression<Func<T, object>> updateExp, params T[] models);

    /// <summary>
    /// 执行sql查询
    /// </summary>
    /// <param name="sql"></param>
    /// <param name="parameters"></param>
    /// <typeparam name="TModel"></typeparam>
    /// <returns></returns>
    Task<List<TModel>> SqlQueryAsync<TModel>(string sql, params SugarParameter[] parameters);

    public void BeginTran();

    public void CommitTran();

    public void RollbackTran();
}