﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using SqlSugar;

namespace Repository;

/// <summary>
/// 文档： https://www.donet5.com/Home/Doc?typeId=1191
/// </summary>
/// <typeparam name="T"></typeparam>
public class BaseRepository<T> : SimpleClient<T>, IBaseRepository<T> where T : class, new()
{
    public BaseRepository(ISqlSugarClient db) : base(db)
    {
    }

    #region 扩展改
    public async Task<int> UpdateAsync(Expression<Func<T, object>> selectExp = null, params T[] models)
    {
        return await AsUpdateable(models).UpdateColumnsIF(selectExp != null, selectExp).ExecuteCommandAsync();
    }

    public async Task<int> UpdateAsync(Expression<Func<T, object>> whereExp, Expression<Func<T, object>> updateExp, params T[] models)
    {
        return await AsUpdateable(models).WhereColumns(whereExp).UpdateColumns(updateExp).ExecuteCommandAsync();
    }
    #endregion

    #region 扩展查
    public async Task<List<TModel>> SqlQueryAsync<TModel>(string sql, params SugarParameter[] parameters)
    {
        return await AsSugarClient().Ado.SqlQueryAsync<TModel>(sql, parameters);
    }

    public async Task<TModel> SqlQuerySingleAsync<TModel>(string sql, params SugarParameter[] parameters)
    {
        return await AsSugarClient().Ado.SqlQuerySingleAsync<TModel>(sql, parameters);
    }
    #endregion
    #region 事务
    public void BeginTran()
    {
        AsTenant().BeginTran();
    }

    public void CommitTran()
    {
        AsTenant().CommitTran();
    }

    public void RollbackTran()
    {
        AsTenant().RollbackTran();
    }
    #endregion
}