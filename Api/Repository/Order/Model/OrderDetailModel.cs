﻿namespace Repository;

public class OrderDetailModel
{
    public string OrderNumber { get; set; }
    public decimal TotalPrice { get; set; }
    public DateTime CreateTime { get; set; }
    public IEnumerable<OrderDetailItemModel> Items { get; set; }
}

public class OrderDetailItemModel
{
    public string GoodName { get; set; }
    public decimal Price { get; set; }
    public int Count { get; set; }
}