﻿namespace Repository;

public class OrderSearchModel
{
    public string OrderNumber { get; set; }
    public string Desc { get; set; }
    public decimal TotalPrice { get; set; }
}