﻿using SqlSugar;
using Model;
using Repository;

namespace Repository;

public class OrderDetailRepository : IDbAccess
{
    private readonly OrderRepository _orderRepository;
    private readonly OrderItemRepository _itemRepository;

    public OrderDetailRepository(OrderRepository orderRepository, OrderItemRepository itemRepository)
    {
        _orderRepository = orderRepository;
        _itemRepository = itemRepository;
    }

    public async Task<(OrderItem? i, Order? o)> GetOrderItem(int orderItemId)
    {
        var obj = await _itemRepository.AsQueryable()
            .LeftJoin<Order>((i, o) => i.OrderId == o.Id)
            .Where((i, o) => i.Id == orderItemId)
            .Select((i, o) => new { i, o })
            .FirstAsync();
        return (obj?.i, obj?.o);
    }

    public async Task<List<OrderDetailModel>> GetOrderDetails(OrderSearchModel searchModel)
    {
        return await Task.FromResult(
            _orderRepository.AsQueryable()
                .WhereIF(!string.IsNullOrEmpty(searchModel.Desc), (o) => searchModel.Desc.Contains(o.Desc))
                .WhereIF(!string.IsNullOrEmpty(searchModel.OrderNumber), (o) => o.OrderNumber == searchModel.OrderNumber)
                .Includes(p => p.OrderItems)
                .ToList(o => new OrderDetailModel
                {
                    OrderNumber = o.OrderNumber,
                    TotalPrice = o.OrderItems.Sum(z => z.Price * z.Count),
                    CreateTime = o.CreateTime,
                    Items = o.OrderItems.Select(p => new OrderDetailItemModel
                    {
                        GoodName = p.GoodName,
                        Price = p.Price,
                        Count = p.Count
                    }),
                })
        );
    }
}