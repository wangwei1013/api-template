﻿using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Serilog;
using Extension;
using Model;

namespace Extension;

public class EmailResponseHandler : INotificationHandler<EmailResponseMessage>
{
    public Task Handle(EmailResponseMessage notification, CancellationToken cancellationToken)
    {
        var msg = $"邮件发送回调：{JsonConvert.SerializeObject(notification)}";
        Log.Information(msg);
        return Task.CompletedTask;
    }
}

public class EmailRequestHandler : INotificationHandler<EmailRequestMessage>
{
    private readonly IServiceProvider _serviceProvider;

    public EmailRequestHandler(IServiceProvider serviceProvider)
    {
        _serviceProvider = serviceProvider;
    }

    public async Task Handle(EmailRequestMessage notification, CancellationToken cancellationToken)
    {
        using var serviceScope = _serviceProvider.CreateScope();
        var emailService = serviceScope.ServiceProvider.GetService<IEmailServices>();
        if (emailService is null)
        {
            throw new Exception("不能找到接口“IEmailServices”的实现");
        }
        await emailService.SendAsync(notification);
    }
}