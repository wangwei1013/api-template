﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Serilog;

namespace Extension;

public static class CorsExtension
{
    private const string CorsName = "CorsPolicy";

    public static IServiceCollection AddCustomCors(this IServiceCollection services)
    {
        services.AddCors(policy =>
        {
            policy.AddPolicy(CorsName, opt =>
                opt.AllowAnyOrigin()
                    .AllowAnyHeader()
                    .AllowAnyMethod()
            );
        });
        return services;
    }

    public static WebApplication UseCustomCors(this WebApplication app)
    {
        app.UseCors(CorsName);
        return app;
    }
}