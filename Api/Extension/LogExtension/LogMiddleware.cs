﻿using System.Diagnostics;
using System.Text;
using Microsoft.AspNetCore.Http;
using Model;
using Newtonsoft.Json;
using Serilog;

namespace Extension.LogExtension;

public class LogMiddleware
{
    private readonly RequestDelegate _next;

    public LogMiddleware(RequestDelegate next)
    {
        _next = next;
    }

    public async Task Invoke(HttpContext context)
    {
        var method = context.Request.Method;
        var url = GetAbsoluteUri(context.Request);
        context.Request.EnableBuffering();
        using var streamReader = new StreamReader(context.Request.Body, Encoding.UTF8, detectEncodingFromByteOrderMarks: false, leaveOpen: true);
        var body = await streamReader.ReadToEndAsync();
        context.Request.Body.Position = 0;
        var watch = Stopwatch.StartNew();
        var isOk = true;
        try
        {
            await _next.Invoke(context);
        }
        catch (Exception e)
        {
            isOk = false;
            await HandleError(context, e);
        }
        var milliseconds = watch.ElapsedMilliseconds;
        watch.Stop();
        StringBuilder sb = new StringBuilder();
        sb.Append($"{method.ToUpper()} {url} {milliseconds}ms -- ");
        sb.Append(isOk ? "success" : "failed");
        sb.Append(Environment.NewLine);
        sb.Append(body.Replace("\n", "").Replace(" ", "").Replace("\t", "").Replace("\r", ""));
        Log.Information(sb.ToString());
    }

    public string GetAbsoluteUri(HttpRequest request)
    {
        return new StringBuilder()
            .Append(request.Scheme)
            .Append("://")
            .Append(request.Host)
            .Append(request.PathBase)
            .Append(request.Path)
            .Append(request.QueryString)
            .ToString();
    }

    /// <summary>
    /// 错误信息处理方法
    /// </summary>
    /// <param name="context"></param>
    /// <param name="ex"></param>
    /// <returns></returns>
    private async Task HandleError(HttpContext context, Exception ex)
    {
        #if DEBUG
        context.Response.StatusCode = 500;
        string message = ex.Message;
        #else
        context.Response.StatusCode = 200;
        string message = "服务器错误,请联系管理员";
        #endif
        context.Response.ContentType = "text/json;charset=utf-8;";
        // string errorMsg = $"错误消息:{ex.Message}{Environment.NewLine}错误追踪:{ex.StackTrace}";
        //无论是否为开发环境都记录错误日志
        // _logger.LogError(errorMsg);
        //浏览器在开发环境显示详细错误信息,其他环境隐藏错误信息
        var msg = new ApiReturn
        {
            Code = 500,
            Message = ex.Message,
        };
        var str = JsonConvert.SerializeObject(msg);
        await context.Response.WriteAsync(str);
    }
}