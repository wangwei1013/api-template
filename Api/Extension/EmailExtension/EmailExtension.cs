﻿using Microsoft.Extensions.DependencyInjection;

namespace Extension;

public static class EmailExtension
{
    public static IServiceCollection AddCustomEmail(this IServiceCollection services)
    {
        services.AddTransient<IEmailServices, EmailServices>();
        return services;
    }
}