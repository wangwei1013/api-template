﻿namespace Extension;

public class EmailConfigModel
{
    public static string Position = "Email";
    public string? SmtpServer { get; set; }
    public int SmtpPort { get; set; }
    public bool EnableSsl { get; set; }
    public int Timeout { get; set; }
    public string? UserName { get; set; }
    public string? Password { get; set; }
}