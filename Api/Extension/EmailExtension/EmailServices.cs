﻿using MailKit.Net.Smtp;
using MailKit.Security;
using MediatR;
using Microsoft.Extensions.Options;
using MimeKit;
using MimeKit.Text;
using Model;

namespace Extension;

public interface IEmailServices
{
    Task SendAsync(EmailRequestMessage model);
}

public class EmailServices : IEmailServices
{
    private readonly EmailConfigModel _emailConfigModel;
    private readonly IMediator _mediator;

    public EmailServices(IOptions<EmailConfigModel> emailConfig, IMediator mediator)
    {
        _mediator = mediator;
        _emailConfigModel = emailConfig.Value;
    }

    public async Task SendAsync(EmailRequestMessage model)
    {
        if (model.To.Count < 1)
        {
            throw new Exception("收件人不能为null");
        }
        var email = new MimeMessage();
        email.From.Add(MailboxAddress.Parse(model.From ?? _emailConfigModel.UserName));
        foreach (var item in model.To)
        {
            email.To.Add(MailboxAddress.Parse(item));
        }
        email.Subject = model.Subject;
        email.Body = new TextPart(TextFormat.Plain) { Text = model.Body };

        using var smtp = new SmtpClient();
        await smtp.ConnectAsync(_emailConfigModel.SmtpServer, _emailConfigModel.SmtpPort, SecureSocketOptions.SslOnConnect);
        await smtp.AuthenticateAsync(_emailConfigModel.UserName, _emailConfigModel.Password);
        smtp.Timeout = _emailConfigModel.Timeout;
        var status = await smtp.SendAsync(email);
        await _mediator.Publish(new EmailResponseMessage(model.EmailId, status.StartsWith("Ok")));
    }
} 
 