﻿using Model;

namespace Extension;

public class JwtConfigModel
{
    public static string Position = "Jwt";
    public string Issuer { get; set; }
    public string SignKey { get; set; }
}