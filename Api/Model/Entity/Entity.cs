﻿using SqlSugar;

namespace Model;

public class IdEntity : Entity
{
    [SugarColumn(IsPrimaryKey = true, IsIdentity = true)]
    public int Id { get; set; }
}

public class Entity
{
}