﻿using System;
using System.Collections.Generic;
using SqlSugar;

namespace Model;

[SugarTable("sysUser")]
public class SysUser : IdEntity
{
    /// <summary>
    /// 用户姓名
    /// </summary>
    [SugarColumn()]
    public string UserName { get; set; }

    /// <summary>
    /// 用户账号
    /// </summary>
    [SugarColumn()]
    public string UserAccount { get; set; }

    /// <summary>
    /// 用户密码
    /// </summary>
    [SugarColumn()]
    public string UserPwd { get; set; }


    /// <summary>
    /// 是否删除，1 是，0 否
    /// </summary>
    [SugarColumn()]
    public int IsDelete { get; set; }

    /// <summary>
    /// 创建时间
    /// </summary>
    [SugarColumn(IsOnlyIgnoreUpdate = true)]
    public DateTime CreateTime { get; set; }

    /// <summary>
    /// 最后登录时间
    /// </summary>
    [SugarColumn()]
    public DateTime? LastLoginTime { get; set; }

    /// <summary>
    /// 创建人
    /// </summary>
    [SugarColumn(IsOnlyIgnoreUpdate = true)]
    public int CreateUserId { set; get; }

    /// <summary>
    /// 用户角色
    /// </summary>
    [Navigate(typeof(SysUserRole), nameof(SysUserRole.UserId), nameof(SysUserRole.RoleId))]
    public List<SysRole> UserRoles { get; set; }
}