﻿using SqlSugar;

namespace Model;

[SugarTable("Orders")]
public class Order : IdEntity
{
    [SugarColumn(ColumnDescription = "订单编号")]
    public string OrderNumber { get; set; }

    [SugarColumn(ColumnDescription = "订单描述")]
    public string Desc { get; set; }

    [SugarColumn(ColumnDescription = "创建时间")]
    public DateTime CreateTime { get; set; }

    [SugarColumn(ColumnDescription = "修改时间")]
    public DateTime UpdateTime { get; set; }

    [Navigate(NavigateType.OneToMany, nameof(OrderItem.OrderId))]
    public List<OrderItem> OrderItems { get; set; }

    public Order()
    {
        CreateTime = DateTime.Now;
        OrderNumber = string.Concat("O", DateTime.Now.Ticks);
        UpdateTime = CreateTime;
    }

    public void ModifyUpdateTime()
    {
        UpdateTime = DateTime.Now;
    }
}