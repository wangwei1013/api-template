﻿using SqlSugar;

namespace Model;

[SugarTable("OrderItems")]
public class OrderItem : IdEntity
{
    [SugarColumn(ColumnDescription = "订单表Id")]
    public int OrderId { get; set; }

    [SugarColumn(ColumnDescription = "商品名称")]
    public string GoodName { get; set; }

    [SugarColumn(ColumnDescription = "商品编号")]
    public string GoodNumber { get; set; }

    [SugarColumn(Length = 11, DecimalDigits = 2, ColumnDescription = "商品价格")]
    public decimal Price { get; set; }

    [SugarColumn(ColumnDescription = "购买数量")]
    public int Count { get; set; }

    [SugarColumn(ColumnDescription = "创建时间")]
    public DateTime CreateTime { get; set; }

    [SugarColumn(ColumnDescription = "修改时间")]
    public DateTime UpdateTime { get; set; }


    public OrderItem()
    {
        CreateTime=DateTime.Now;
        UpdateTime = CreateTime;
    }
    
    public void ModifyItem(string goodName = null, string goodNumber = null, decimal? price = null, int? count = null)
    {
        if (!string.IsNullOrWhiteSpace(goodName))
        {
            GoodName = goodName;
        }
        if (!string.IsNullOrWhiteSpace(goodNumber))
        {
            GoodNumber = goodNumber;
        }
        if (price != null)
        {
            Price = price.Value;
        }
        if (count is > 0)
        {
            Count = count.Value;
        }
        UpdateTime = DateTime.Now;
    }
}