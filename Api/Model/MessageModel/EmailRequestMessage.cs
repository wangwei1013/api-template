﻿using MediatR;

namespace Model;

public class EmailRequestMessage : INotification
{
    public string EmailId { get; set; } = Guid.NewGuid().ToString();
    public readonly string? From;
    private readonly EmailTemplateType _emailTemplateType;
    public readonly List<string> To;
    public readonly string Subject;
    private readonly string[] _args;

    public EmailRequestMessage(List<string> to, string subject, EmailTemplateType emailTemplateType, string? from = null, params string[] args)
    {
        From = from;
        _emailTemplateType = emailTemplateType;
        _args = args;
        To = to;
        Subject = subject;
    }

    public string Body => EmailTemplate.GetTemplateContent(_emailTemplateType, _args);
}