﻿using MediatR;

namespace Model;

public class EmailResponseMessage : INotification
{
    public EmailResponseMessage(string emailId,bool isOk)
    {
        EmailId = emailId;
        IsOk = isOk;
    }

    public readonly string EmailId;
    public readonly bool IsOk;
}