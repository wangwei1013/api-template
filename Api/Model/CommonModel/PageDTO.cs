﻿namespace Model;

public class PageDTO
{
    private int _pageSize;
    private int _pageIndex;


    public int PageIndex
    {
        get => _pageIndex;
        set => _pageIndex = value < 1 ? 1 : value;
    }

    public int PageSize
    {
        get => _pageSize;
        set => _pageSize = value < 10 ? 10 : value;
    }
}