using System.Text.Json;
using Newtonsoft.Json;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using SqlSugar;
using Application;
using Extension.LogExtension;
using Extension;
using Repository;

var builder = WebApplication.CreateBuilder(args);
// Add services to the container.  
builder.Services.AddControllers()
    .AddNewtonsoftJson(opt =>
    {
        opt.SerializerSettings.Converters.Add(new StringEnumConverter());
        opt.SerializerSettings.DateFormatString = "yyyy-MM-dd HH:mm:ss";
        opt.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
    })
    .AddJsonOptions(opt => { opt.JsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase; });
builder.Services.AddConfig(builder.Configuration);
builder.Services.AddMemoryCache();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddCustomJwt(builder.Configuration);
builder.Services.AddCustomSerilog();
builder.Services.AddCustomMediatR();
builder.Services.AddCustomSwagger();
builder.Services.AddCustomCors();
builder.Services.AddCustomSqlSugar(builder.Configuration);
builder.Services.AddCustomEmail();
builder.Services.AddAppServices();
builder.WebHost.ConfigureKestrel(opts => { opts.AddServerHeader = true; });

var app = builder.Build();
app.UseCustomSwagger();
app.UseCustomCors();
// app.UseHttpsRedirection();
app.UseCustomJwt();
app.UseMiddleware<LogMiddleware>();
app.UseMiddleware<MenuAuthorizationMidd>();
app.MapControllers();
app.Run(builder.Configuration["ApplicationUrl"]);