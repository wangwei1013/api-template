﻿ 
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Model;

namespace Api;

[ApiController]
[Route("api/[controller]")]
[Produces("application/json")]
public class BaseController : ControllerBase
{
    protected IActionResult RenderSuccess<T>(T data) =>
        new JsonResult(new ApiReturn<T>(200, string.Empty, data));

    protected IActionResult RenderError(string msg) =>
        new JsonResult(new ApiReturn(500, msg));

    protected IActionResult RenderError(Exception ex) =>
        new JsonResult(new ApiReturn(500, ex.Message));

    protected IActionResult RenderResult<T>(ApiReturn<T> m) =>
        new JsonResult(m);

    protected IActionResult RenderResult(string msg)
    {
        if (string.IsNullOrEmpty(msg))
            return RenderSuccess<string>(string.Empty);
        return RenderError(msg);
    }

    protected IActionResult RenderResult(bool success, string errMsg = "") =>
        new JsonResult(new ApiReturn<bool>(success ? 200 : 500, errMsg, success));

    protected IActionResult RenderPage<T>(List<T> data, int total) =>
        new JsonResult(new ApiReturnPage<T>(data, total));
}