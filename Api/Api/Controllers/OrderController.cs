﻿using System.Net;
using Microsoft.AspNetCore.Mvc;
using SqlSugar;
using Application;
using Model;

namespace Api.Controllers;

public class OrderController : BaseController
{
    private readonly OrderAppService _orderAppService;

    public OrderController(OrderAppService orderAppService)
    {
        _orderAppService = orderAppService;
    }


    /// <summary>
    /// 获取订单列表
    /// </summary>
    /// <param name="dto"></param>
    /// <returns></returns>
    [HttpPost("[action]")]
    [ProducesResponseType(typeof(ApiReturnPage<GetOrderOutputDTO>), 200)]
    public async Task<IActionResult> GetOrders(GetOrderInputDTO dto)
    {
        var page = dto.GetPageModel();
        var list = await _orderAppService.GetOrders(dto.ToSearchModel(), page);
        return RenderPage(list, page.TotalCount);
    }

    /// <summary>
    /// 添加订单
    /// </summary>
    /// <param name="dto"></param>
    /// <returns></returns>
    [HttpPost("[action]")]
    [ProducesResponseType(typeof(ApiReturn<bool>), 200)]
    public async Task<IActionResult> AddOrder(List<AddOrderInputDTO> dto)
    {
        var res = await _orderAppService.AddOrder(dto);
        return RenderResult(res);
    }

    /// <summary>
    /// 修改订单
    /// </summary>
    /// <param name="dto"></param>
    /// <returns></returns>
    [HttpPost("[action]")]
    [ProducesResponseType(typeof(ApiReturn<bool>), 200)]
    public async Task<IActionResult> ModifyOrder(ModifyOrderInputDTO dto)
    {
        if (dto.OrderItemId == default)
        {
            return RenderError("参数错误");
        }
        var res = await _orderAppService.ModifyOrder(dto);
        return RenderResult(res);
    }

    /// <summary>
    /// 删除订单
    /// </summary>
    /// <param name="dto"></param>
    /// <returns></returns>
    [HttpPost("[action]")]
    [ProducesResponseType(typeof(ApiReturn<bool>), 200)]
    public async Task<IActionResult> DeleteOrder(DeleteOrderInputDTO dto)
    {
        if (dto.OrderId == default)
        {
            return RenderError("参数错误");
        }
        var res = await _orderAppService.DeleteOrder(dto);
        return RenderResult(res);
    }
}