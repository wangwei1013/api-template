﻿using Application;
using Extension;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Model;

namespace Api.Controllers;

[Authorize]
public class AuthController : BaseController
{
    private readonly JwtHelper _jwtHelper;
    private readonly AuthAppService _appService;

    public AuthController(JwtHelper jwtHelper, AuthAppService appService)
    {
        _jwtHelper = jwtHelper;
        _appService = appService;
    }

    /// <summary>
    /// 登录
    /// </summary> 
    /// <returns></returns>
    [HttpPost("[action]")]
    [ProducesResponseType(typeof(ApiReturn<LoginOutputDTO>), 200)]
    [AllowAnonymous]
    public async Task<IActionResult> Login(LoginInputDTO dto)
    {
        if (string.IsNullOrEmpty(dto.UserAccount) || string.IsNullOrEmpty(dto.Pwd))
        {
            return RenderError("用户名或者密码不能为空");
        }
        var user = await _appService.Login(dto);
        if (!user.Success)
        {
            return RenderError(user.Message);
        }
        var ret = new LoginOutputDTO();
        var generateToken = _jwtHelper.GenerateToken(user.Data.UserAccount, user.Data.Roles, user.Data.Menus);
        ret.UserInfo = user.Data;
        ret.Token = generateToken;
        return RenderSuccess(ret);
    }

    #region 角色授权
    /// <summary>
    /// 管理员权限
    /// </summary>
    /// <returns></returns>
    [HttpGet("[action]")]
    [ProducesResponseType(typeof(ApiReturn<string>), 200)]
    [Authorize(Roles = $"{RoleKeys.Admin}")]
    public async Task<IActionResult> AdminAuth()
    {
        return RenderSuccess("成功");
    }

    /// <summary>
    /// 销售员及以上权限
    /// </summary>
    /// <returns></returns>
    [HttpGet("[action]")]
    [ProducesResponseType(typeof(ApiReturn<string>), 200)]
    [Authorize(Roles = $"{RoleKeys.Salesman},{RoleKeys.Admin}")]
    public async Task<IActionResult> SalesmanAuth()
    {
        return RenderSuccess("成功");
    }

    /// <summary>
    /// 仓管员及以上的权限
    /// </summary>
    /// <returns></returns>
    [HttpGet("[action]")]
    [ProducesResponseType(typeof(ApiReturn<string>), 200)]
    [Authorize(Roles = $"{RoleKeys.Storeman}")]
    public async Task<IActionResult> SotremanAuth()
    {
        return RenderSuccess("成功");
    }
    #endregion

    #region 菜单授权
    /// <summary>
    /// 写订单
    /// </summary>
    /// <returns></returns>
    [HttpGet("[action]")]
    [ProducesResponseType(typeof(ApiReturn<string>), 200)]
    [MenuAuthorization(MenuPermissionLevel.Write, MenuKeys.OrderManagement)]
    public async Task<IActionResult> WriteOrder()
    {
        return RenderSuccess("成功");
    }

    /// <summary>
    /// 写用户
    /// </summary>
    /// <returns></returns>
    [HttpGet("[action]")]
    [ProducesResponseType(typeof(ApiReturn<string>), 200)]
    [MenuAuthorization(MenuPermissionLevel.Write, $"{MenuKeys.UserManagement},{MenuKeys.OrderManagement}")]
    public async Task<IActionResult> WriteUser()
    {
        return RenderSuccess("成功");
    }

    /// <summary>
    /// 写商品
    /// </summary>
    /// <returns></returns>
    [HttpGet("[action]")]
    [ProducesResponseType(typeof(ApiReturn<string>), 200)]
    [MenuAuthorization(MenuPermissionLevel.Write, MenuKeys.GoodsManagement)] 
    public async Task<IActionResult> WriteGoods()
    {
        return RenderSuccess("成功");
    }
    #endregion
}