﻿using MediatR;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Mvc;
using Application;
using Extension;
using Microsoft.AspNetCore.Authorization;
using Model;

namespace Api.Controllers;

public class TestController : BaseController
{
    private readonly IEmailServices _emailServices;
    private readonly IMediator _mediator;

    public TestController(IEmailServices emailServices, IMediator mediator)
    {
        _emailServices = emailServices;
        _mediator = mediator;
    }

    /// <summary>
    /// 异步发邮件
    /// </summary>
    /// <returns></returns>
    [HttpPost("[action]")]
    [ProducesResponseType(typeof(ApiReturn<string>), 200)]
    public async Task<IActionResult> AsyncSendEmail()
    {
        var emailRequestMessageModel = new EmailRequestMessage(new List<string>() { "dreamlongs@foxmail.com" }, "test", EmailTemplateType.HelloWorld, args: "aaa");
        var t = _mediator.Publish(emailRequestMessageModel);
        return RenderSuccess(emailRequestMessageModel.EmailId);
    }

    /// <summary>
    /// 发送邮件，并等待邮件发送结果
    /// </summary>
    /// <returns></returns>
    [HttpPost("[action]")]
    [ProducesResponseType(typeof(ApiReturn<string>), 200)]
    public async Task<IActionResult> SendEmail()
    {
        var emailRequestMessageModel = new EmailRequestMessage(new List<string>() { "dreamlongs@foxmail.com" }, "test", EmailTemplateType.HelloWorld, args: "aaa");
        await _emailServices.SendAsync(emailRequestMessageModel);
        return RenderSuccess(emailRequestMessageModel.EmailId);
    }

    /// <summary>
    /// 路由参数测试
    /// </summary>
    /// <param name="dto"></param>
    /// <returns></returns>
    [HttpPost("[action]/{pageIndex}/{pageSize}")]
    [ProducesResponseType(typeof(ApiReturn<string>), 200)]
    [Authorize(Policy = "write", Roles = "user")]
    public async Task<IActionResult> RouteTest(DeleteOrderInputDTO dto, int pageIndex, int pageSize)
    {
        return RenderSuccess($"{pageIndex}, {pageSize}");
    }

    /// <summary>
    /// 路由参数测试
    /// </summary>
    /// <param name="dto"></param>
    /// <returns></returns>
    [HttpPost("[action]")]
    [ProducesResponseType(typeof(ApiReturn<string>), 200)]
    public async Task<IActionResult> RouteTest2(DeleteOrderInputDTO dto, [FromQuery] int pageIndex, [FromQuery] int pageSize)
    {
        return RenderSuccess($"{pageIndex}, {pageSize}");
    }
}