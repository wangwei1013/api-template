﻿using Model;
using Model.ServiceModel;
using Repository;

namespace Application;

public class AuthAppService : ApplicationService
{
    private readonly UserRepository _userRepository;

    public AuthAppService(UserRepository userRepository)
    {
        _userRepository = userRepository;
    }

    public async Task<ApiReturn<LoginUserInfo>> Login(LoginInputDTO dto)
    {
        var user = await _userRepository
            .AsQueryable()
            .Where(p => p.UserAccount == dto.UserAccount)
            .Includes(p => p.UserRoles, r => r.RoleMenus)
            .Includes(p => p.UserRoles, r => r.Menus)
            .FirstAsync();
        if (user == null)
        {
            return RenderError<LoginUserInfo>("用户不存在");
        }
        if (user.UserPwd != dto.Pwd)
        {
            return RenderError<LoginUserInfo>("用户密码错误");
        }

        return RenderSuccess(LoginUserInfo.FromSysUser(user));
    }
}