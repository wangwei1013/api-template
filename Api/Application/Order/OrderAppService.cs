﻿using SqlSugar;
using Model;
using Repository;

namespace Application;

public class OrderAppService : ApplicationService
{
    private readonly OrderDetailRepository _detailRepository;
    private readonly OrderRepository _orderRepository;
    private readonly OrderItemRepository _itemRepository;

    public OrderAppService(OrderDetailRepository detailRepository, OrderRepository orderRepository, OrderItemRepository itemRepository)
    {
        _detailRepository = detailRepository;
        _orderRepository = orderRepository;
        _itemRepository = itemRepository;
    }

    public async Task<List<GetOrderOutputDTO>> GetOrders(OrderSearchModel searchModel, PageModel pageModel)
    {
        RefAsync<int> total = new RefAsync<int>();

        var list = await _orderRepository.AsQueryable()
            .WhereIF(!string.IsNullOrEmpty(searchModel.OrderNumber), p => p.OrderNumber == searchModel.OrderNumber)
            .LeftJoin<OrderItem>((o, i) => o.Id == i.OrderId)
            .GroupBy((o, i) => o.Id)
            .OrderBy((o, i) => o.CreateTime, OrderByType.Desc)
            .Select((o, i) => new GetOrderOutputDTO
            {
                OrderNumber = o.OrderNumber,
                Desc = o.Desc,
                TotalPrice = SqlFunc.AggregateSum(i.Price * i.Count),
                CreateTime = o.CreateTime,
                UpdateTime = o.UpdateTime
            })
            .ToPageListAsync(pageModel.PageIndex, pageModel.PageSize, total);
        pageModel.TotalCount = total;
        return list;
    }

    public async Task<bool> AddOrder(List<AddOrderInputDTO> dto)
    {
        Order order = new Order();
        List<OrderItem> orderItems = new List<OrderItem>();
        foreach (var item in dto)
        {
            orderItems.Add(item.ToOrderItem());
            order.Desc += item.GoodName + ";";
        }
        _orderRepository.BeginTran();
        try
        {
            var orderId = await _orderRepository.InsertReturnIdentityAsync(order);
            orderItems.ForEach(p => p.OrderId = orderId);
            var isOk = await _itemRepository.InsertRangeAsync(orderItems);
            if (isOk)
            {
                _orderRepository.CommitTran();
                return true;
            }
            _orderRepository.RollbackTran();
            return false;
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            _orderRepository.RollbackTran();
            return false;
        }
    }

    public async Task<ApiReturn<bool>> ModifyOrder(ModifyOrderInputDTO dto)
    {
        var (orderItem, order) = await _detailRepository.GetOrderItem(dto.OrderItemId);
        if (orderItem is null || order is null)
        {
            return RenderError<bool>("未能找到对应订单");
        }
        orderItem.ModifyItem(dto.GoodName, dto.GoodNumber, dto.Price, dto.Count);
        order.ModifyUpdateTime();
        try
        {
            _itemRepository.BeginTran();
            await _itemRepository.UpdateAsync(orderItem);
            await _orderRepository.UpdateAsync(p => new { p.UpdateTime }, order);
            _itemRepository.CommitTran();
            return RenderSuccess(true);
        }
        catch (Exception e)
        {
            _itemRepository.RollbackTran();
            return RenderError<bool>("修改失败");
        }
    }

    public async Task<ApiReturn<bool>> DeleteOrder(DeleteOrderInputDTO dto)
    {
        var exist = await _orderRepository.IsAnyAsync(p => p.Id == dto.OrderId);
        if (!exist)
        {
            return RenderError<bool>("订单不存在，无法删除");
        }

        try
        {
            _itemRepository.BeginTran();
            await _itemRepository.DeleteAsync(p => p.OrderId == dto.OrderId);
            await _orderRepository.DeleteAsync(p => p.Id == dto.OrderId);
            _itemRepository.CommitTran();
            return RenderSuccess(true);
        }
        catch (Exception e)
        {
            _itemRepository.RollbackTran();
            return RenderError<bool>("删除失败");
        }
    }
}