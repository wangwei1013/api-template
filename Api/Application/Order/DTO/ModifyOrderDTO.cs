﻿namespace Application;

public class ModifyOrderInputDTO
{
    public int OrderItemId { get; set; }
    public string GoodName { get; set; }
    public string GoodNumber { get; set; }
    public decimal? Price { get; set; }
    public int? Count { get; set; }
}