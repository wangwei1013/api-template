﻿using SqlSugar;
using Model;
using Repository;

namespace Application;

public class GetOrderInputDTO : PageDTO
{
    public string OrderNumber { get; set; }
    public string Desc { get; set; }

    public OrderSearchModel ToSearchModel() => new OrderSearchModel
    {
        OrderNumber = OrderNumber,
        Desc = Desc
    };

    public PageModel GetPageModel() => new PageModel
    {
        PageIndex = PageIndex,
        PageSize = PageSize
    };
}

public class GetOrderOutputDTO
{
    public string OrderNumber { get; set; }

    public string Desc { get; set; }
    public decimal TotalPrice { get; set; }

    public DateTime CreateTime { get; set; }

    public DateTime UpdateTime { get; set; }
}