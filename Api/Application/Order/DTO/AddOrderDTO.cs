﻿using Model;

namespace Application;

public class AddOrderInputDTO
{
    public string GoodName { get; set; }
    public string GoodNumber { get; set; }
    public decimal Price { get; set; }
    public int Count { get; set; }

    public OrderItem ToOrderItem() => new OrderItem
    {
        GoodName = GoodName,
        GoodNumber = GoodNumber,
        Price = Price,
        Count = Count
    };
}