﻿namespace Application;

public class DeleteOrderInputDTO
{
    public int OrderId { get; set; }
}