﻿using Model;

namespace Application;

public abstract class ApplicationService
{
    protected ApiReturn<T> RenderSuccess<T>(T data) =>
        new ApiReturn<T>(200, string.Empty, data);

    protected ApiReturn<T?> RenderError<T>(string msg) => new(500, msg, default);
}